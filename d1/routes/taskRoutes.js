const express = require("express");
const router = express.Router();

const TaskController = require('../controllers/taskControllers')

//get all the tasks 
router.get("/", (req, res) => {
	TaskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})


//Creating a task
router.post("/", (req,res) => {
	TaskController.createTask(req.body).then(result => res.send(result));
})

//delete a task
router.delete("/:id", (req, res) => {
	TaskController.deleteTask(req.params.id).then(result => res.send(result));
})

//Update a task
router.put("/:id", (req, res) => {
	TaskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})





module.exports = router;
