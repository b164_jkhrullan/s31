const Task = require('../models/task');

//controller function for getting all the tasks

module.exports.getAllTasks = () => {
	return Task.find({}).then( result => {
		return result; 
	})
};

//Creating a task
module.exports.createTask = (requestBody) => {

	//Create object
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error);
			return false;
		} else{
			return task;
		}
	})
}

//Deleting a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} 

		result.name = newContent.name; 

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})

	})
}

















